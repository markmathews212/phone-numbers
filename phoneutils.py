from py4j.java_gateway import JavaGateway


class PhoneUtils:
    def __init__(self):
        self.gateway = JavaGateway()    # connect to the JVM
        self.phoneutils_app = self.gateway.entry_point  # get the PhneUtils instance

    def validatePhoneNumber(self, number):
        return self.phoneutils_app.ValidatePhoneNum( number )

