from py4j.java_gateway import JavaGateway
from phoneutils import *
import sys

def getNumber( args):
    if( len( args ) > 1 ):
        return "" + args[1]
    else:
        return "0"

if __name__ == '__main__':
    putils = PhoneUtils()
    phoneNumber = getNumber( sys.argv )
    print( phoneNumber )
    result = putils.validatePhoneNumber( phoneNumber )
    print( "Phone number: " + phoneNumber + " is " + result )


